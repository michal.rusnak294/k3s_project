# Control node
resource "aws_instance" "control" {
  ami             = var.image
  instance_type   = var.instance
  subnet_id       = aws_subnet.dmz.id
  private_ip      = "10.0.0.69"
  user_data       = file("postinstall-m.sh")
  vpc_security_group_ids = [aws_security_group.allow_http_ssh.id]

  tags = {
    Owner = var.owner
    Name  = "control"
  }
}

# Worker node 1
resource "aws_instance" "worker1" {
  ami             = var.image
  instance_type   = var.instance
  subnet_id       = aws_subnet.dmz.id
  private_ip      = "10.0.0.68"
  user_data       = file("postinstall-w.sh")
  vpc_security_group_ids = [aws_security_group.allow_http_ssh.id]

  tags = {
    Owner = var.owner
    Name  = "worker1"
  }
}

# Worker node 2
resource "aws_instance" "worker2" {
  ami             = var.image
  instance_type   = var.instance
  subnet_id       = aws_subnet.dmz.id
  private_ip      = "10.0.0.67"
  user_data       = file("postinstall-w.sh")
  vpc_security_group_ids = [aws_security_group.allow_http_ssh.id]

  tags = {
    Owner = var.owner
    Name  = "worker2"
  }
}

# Worker node 3
resource "aws_instance" "worker3" {
  ami             = var.image
  instance_type   = var.instance
  subnet_id       = aws_subnet.dmz.id
  private_ip      = "10.0.0.66"
  user_data       = file("postinstall-w.sh")
  vpc_security_group_ids = [aws_security_group.allow_http_ssh.id]

  tags = {
    Owner = var.owner
    Name  = "worker3"
  }
}

