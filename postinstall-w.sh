#!/bin/bash
###############################################################

## K3S - added
# Install k3s - added
curl -sfL https://get.k3s.io | sh -

# Start the agent - added
systemctl start k3s-agent

# Get the token from the environment variable - added (this will only works if nodes are on same network ???)
join_token=$K3S_TOKEN

# Get the master IP from the environment variable - added (this will only works if nodes are on same network ???)
master_ip=$MASTER_IP

# Join the cluster - added
k3s agent --server https://$MASTER_IP:6443 --token $K3S_TOKEN

###############################################################


