#!/bin/bash
###############################################################

## K3S - added
# Install k3s - added
curl -sfL https://get.k3s.io | sh -

# Start the server - added
systemctl start k3s

# Get the join token - added
join_token=$(sudo cat /var/lib/rancher/k3s/server/node-token)

# Set the token as an environment variable - added (this will only works if nodes are on same network...maybe)
echo "export K3S_TOKEN='${join_token}'" >> /etc/environment

# Get the master IP - added
master_ip=$(ip -4 addr show ens3 | grep -oP '(?<=inet\s)\d+(\.\d+){3}')

# Set the master IP as an environment variable - added (this will only works if nodes are on same network...maybe)
echo "export MASTER_IP='${master_ip}'" >> /etc/environment

# Print the join command - added
echo "Run the following command on worker nodes: k3s agent --server https://${master_ip}:6443 --token ${join_token}"

###############################################################

